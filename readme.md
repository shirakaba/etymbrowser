# Usage
After running via the instructions below, follow the in-app instructions to operate. Thorough explanation of the nature of the program is given in report.pdf. Given the predominantly graphical nature of this program, the 'tests' are very limited and not a focus of the submission, existing only to show the possibilities for future connectability rather than measure the reliability of the code.

# Dependencies

## Libraries

MigLayout 3.7.4
JSoup 1.8.3
Junit 4.12
(all included in the `lib` folder for javac use; although compiling via Maven will grab them from straight from the online source specified in pom.xml).

## Java

I recommend compiling and running with JDK 1.8. 

# Compilation

## Via `javac`

`cd` to the root (the directory containing the `src` folder), then:

```shell
javac -sourcepath ./src/main/java -classpath "lib/*" src/main/java/uk/co/birchlabs/etymbrowser/App.java
```

## Via [Maven](https://maven.apache.org/)

Install [Maven](https://maven.apache.org/),`cd` to root (the directory containing `pom.xml`), then:

```shell
mvn -Dmaven.test.skip=true package
```

# Running

## If compiled using `javac`

`cd` to root (the directory containing the `src` folder), then:

```shell
java -cp lib/*:src/main/java uk.co.birchlabs.etymbrowser.App
```

## If compiled using `maven`

Classes will be output to the `target` folder; please `cd` into this folder and:

```shell
java -jar core-1-jar-with-dependencies.jar
```

(Please ignore the similar core-1.jar file generated, which does not have the dependencies bundled into it).

# Running tests

Install [Maven](https://maven.apache.org/), `cd` to root (the directory containing `pom.xml`), then:

```shell
mvn test
```
Note: the 'tests' are merely crude uses of JSoup to run println() outputs to showcase the ability to collect data (and, in future, integrate it into the core app to be displayed) from alternative web sources; they do not 'test' the operation of the core program, per se.
