package uk.co.birchlabs.etymbrowser;

import javax.swing.*;
import java.awt.*;

/** This class creates a JLabel that wraps as its available size changes.
 */
public class WrappingJLabel extends JLabel {
    public WrappingJLabel(String text) {
        super("<html>" + text + "</html>");
        setPreferredSize(new Dimension(1,1));
        setVerticalAlignment(SwingConstants.TOP);
    }
}
