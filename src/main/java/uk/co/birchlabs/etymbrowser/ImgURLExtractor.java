package uk.co.birchlabs.etymbrowser;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.lang.UnsupportedOperationException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/** This class provides several methods through which one can extract the image URLs for Chinese character evolution on
 *  various different websites. Ultimately, the XiaoXue website had the most comprehensive dataset, so the extractors
 *  for Richard Sears' website and Wikimedia are left operational (see tests) yet unintegrated into the final program.
 */
public class ImgURLExtractor {
    private final String kanjiToSearch;
    private static final int GENEROUS_TIMEOUT = 20000;
    private static final String MAX_RESULTS = "36";

    public ImgURLExtractor(String kanjiToSearch) {
        this.kanjiToSearch = kanjiToSearch;
    }


    /** Sourced from Richard Sears' infamous Chinese character etymology website (URL listed in Jsoup.connect()).
     *
     *  Returns a list of URLs to images from four different script categories: Small seal; big seal; bronze; and oracle
     *  bone. */
    public List<String> getGIFsFromSears(Script script) throws IOException {
        Elements target;
        List<String> gifsFound = new ArrayList<>();

        Document doc = Jsoup.connect("http://internationalscientific.org/CharacterEtymology.aspx?characterInput="
                + kanjiToSearch).timeout(GENEROUS_TIMEOUT).get();

        switch (script) {
            case SMALL_SEAL:
                target = doc.select("#SealImages img");
                break;
            case LST_BIG_SEAL:
                target = doc.select("#LstImages img");
                break;
            case BRONZE:
                target = doc.select("#BronzeImages img");
                break;
            case ORACLE:
                target = doc.select("#OracleImages img");
                break;
            default:
                throw new UnsupportedOperationException();
        }

        for (Element element : target) {
            gifsFound.add(element.attr("abs:src"));
        }

        return gifsFound;
    }

    /** Sourced from Wikimedia (URL listed in Jsoup.connect()).
     *
     *  Returns a list of uncategorised, yet chronologically ordered, URLs to images from a varying number of script
     *  categories. Currently gets the images at 1x scale, though other scales are available. */
    public List<String> getPNGsFromWikimedia() throws IOException {
        Document doc = Jsoup.connect("https://commons.wikimedia.org/wiki/Category:ACC_containing_"
                + kanjiToSearch).timeout(GENEROUS_TIMEOUT).get();
        Elements target = doc.select(".wikitable a").select("img");
        List<String> SVGsFound = new ArrayList<>();

        for (Element element : target) {
            SVGsFound.add(element.attr("abs:src"));
        }

        return SVGsFound;
    }

    /** This is a mock response of searching XiaoXue for '馬'. */
    private Document mockResponseFromXiaoXue(){
        return Jsoup.parse(
                "<html>\n" +
                        " <head>\n" +
                        "  <meta charset=\"UTF-16\">\n" +
                        " </head>\n" +
                        " <body>\n" +
                        "  <form id=\"HiddenFrom\" action=\"/yanbian/PageResult/PageResult\" method=\"post\" onclick=\"Sys.Mvc.AsyncForm.handleClick(this, new Sys.UI.DomEvent(event));\" onsubmit=\"Sys.Mvc.AsyncForm.handleSubmit(this, new Sys.UI.DomEvent(event), { insertionMode: Sys.Mvc.InsertionMode.replace, httpMethod: 'Post', updateTargetId: 'PageResult', onSuccess: Function.createDelegate(this, Feedback) });\">\n" +
                        "   <input id=\"ZiOrder\" name=\"ZiOrder\" type=\"hidden\" value=\"\"> \n" +
                        "   <input id=\"EudcFontChar\" name=\"EudcFontChar\" type=\"hidden\" value=\"馬\"> \n" +
                        "   <input id=\"PageNo\" name=\"PageNo\" type=\"hidden\" value=\"\"> \n" +
                        "   <input id=\"Reference\" name=\"Reference\" type=\"hidden\" value=\"\"> \n" +
                        "   <input id=\"IsZiTou\" name=\"IsZiTou\" type=\"hidden\" value=\"0\"> \n" +
                        "   <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"95%\" id=\"yanbian_result\"> \n" +
                        "    <tbody>\n" +
                        "     <tr> \n" +
                        "      <td colspan=\"10\" align=\"left\"> \n" +
                        "       <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> \n" +
                        "        <tbody>\n" +
                        "         <tr> \n" +
                        "          <td class=\"ZiList\"><span id=\"StartOrder\">6104</span></td> \n" +
                        "          <td><img src=\"/ImageText2/ShowImage.ashx?text=%e9%a6%ac&amp;font=%e6%a8%99%e6%a5%b7%e9%ab%94&amp;size=29&amp;style=regular&amp;color=%23000000\" alt=\"&amp;0.99AC;\" class=\"charValue\" style=\"vertical-align:text-bottom\"></td> \n" +
                        "         </tr> \n" +
                        "        </tbody>\n" +
                        "       </table> </td> \n" +
                        "     </tr> \n" +
                        "     <tr> \n" +
                        "      <td colspan=\"10\" align=\"right\" class=\"Information\"> 共搜尋到17字，字形大小：<select name=\"ImageSize\" id=\"ImageSize\"> <option value=\"16\">16</option> <option value=\"20\">20</option> <option value=\"24\">24</option> <option value=\"28\">28</option> <option value=\"32\">32</option> <option value=\"36\" selected>36</option> <option value=\"40\">40</option> <option value=\"44\">44</option> <option value=\"48\">48</option> <option value=\"54\">54</option> <option value=\"60\">60</option> <option value=\"66\">66</option> <option value=\"72\">72</option> </select>點 </td> \n" +
                        "     </tr> \n" +
                        "     <tr> \n" +
                        "      <td colspan=\"10\" align=\"center\" valign=\"top\">&nbsp;</td> \n" +
                        "     </tr> \n" +
                        "     <tr>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%8d%b2&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e7%94%b2%e9%aa%a8%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;43.E372;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>鐵2.2(甲)<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%8d%b3&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e7%94%b2%e9%aa%a8%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;43.E373;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>乙9092(甲)<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%8e%82&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e7%94%b2%e9%aa%a8%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;43.E382;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>甲1286(甲)<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%8e%98&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e7%94%b2%e9%aa%a8%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;43.E398;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>林1.23.20(甲)<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListB\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%a2%81&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e9%87%91%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;33.E881;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>戊寅作父丁方鼎(金)<br>商代晚期</td>\n" +
                        "      <td></td>\n" +
                        "     </tr>\n" +
                        "     <tr> \n" +
                        "     </tr>\n" +
                        "     <tr>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%a2%8a&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e9%87%91%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;33.E88A;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>彔伯戈\uF6A5冬簋蓋(金)<br>西周中期</td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%a2%b9&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e9%87%91%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;33.E8B9;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>大\uD851\uDD32馬簠(金)<br>春秋早期</td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%a2%bd&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e9%87%91%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;33.E8BD;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>伯亞臣\uD858\uDE62(金)<br>春秋</td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%a2%be&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e9%87%91%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;33.E8BE;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>妾\uF6A4子\uD85C\uDE92壺(金)<br>戰國晚期</td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListB\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%a3%80&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e9%87%91%e6%96%87%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;33.E8C0;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>燕侯載器(金)<br>戰國</td>\n" +
                        "      <td></td>\n" +
                        "     </tr>\n" +
                        "     <tr> \n" +
                        "     </tr>\n" +
                        "     <tr>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%86%bb&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e6%a5%9a%e7%b3%bb%e7%b0%a1%e5%b8%9b%e6%96%87%e5%ad%97%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;53.E1BB;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>曾150(楚)<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%85%ab&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e6%a5%9a%e7%b3%bb%e7%b0%a1%e5%b8%9b%e6%96%87%e5%ad%97%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;53.E16B;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>包2.30(楚)<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%85%af&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e6%a5%9a%e7%b3%bb%e7%b0%a1%e5%b8%9b%e6%96%87%e5%ad%97%e9%87%8d%e6%96%87%e4%b8%89&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;53.E16F;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>包2.103(楚)<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%a0%9a&amp;font=%e5%8c%97%e5%b8%ab%e5%a4%a7%e8%aa%aa%e6%96%87%e5%b0%8f%e7%af%86&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;27.E81A;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>說文古文<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListB\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%a0%9b&amp;font=%e5%8c%97%e5%b8%ab%e5%a4%a7%e8%aa%aa%e6%96%87%e5%b0%8f%e7%af%86&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;27.E81B;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>說文籀文<br></td>\n" +
                        "      <td></td>\n" +
                        "     </tr>\n" +
                        "     <tr> \n" +
                        "     </tr>\n" +
                        "     <tr>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%e9%a6%ac&amp;font=%e5%8c%97%e5%b8%ab%e5%a4%a7%e8%aa%aa%e6%96%87%e5%b0%8f%e7%af%86&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;27.99AC;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>說文‧馬部<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\"><img src=\"/ImageText2/ShowImage.ashx?text=%ee%aa%8d&amp;font=%e4%b8%ad%e7%a0%94%e9%99%a2%e7%a7%a6%e7%b3%bb%e7%b0%a1%e7%89%98%e6%96%87%e5%ad%97%e9%87%8d%e6%96%87%e4%b8%80&amp;size=36&amp;style=regular&amp;color=%23000000\" alt=\"&amp;71.EA8D;\" class=\"charValue\" style=\"vertical-align:text-bottom\"><br>睡.雜26(秦)<br></td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\">&nbsp;</td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListA\">&nbsp;</td>\n" +
                        "      <td></td>\n" +
                        "      <td align=\"center\" class=\"VariantListB\">&nbsp;</td>\n" +
                        "      <td></td>\n" +
                        "     </tr> \n" +
                        "     <tr> \n" +
                        "      <td colspan=\"10\" align=\"center\" valign=\"top\" style=\"border-style: solid; border-width: 1px 0px 0px 0px; border-color: #808080\"> <p class=\"RemindMessage\">(點選字形可取得字形編輯資訊)</p></td> \n" +
                        "     </tr> \n" +
                        "     <tr> \n" +
                        "      <td colspan=\"10\" align=\"center\" valign=\"top\">&nbsp;</td> \n" +
                        "     </tr> \n" +
                        "    </tbody>\n" +
                        "   </table> \n" +
                        "  </form> \n" +
                        "  <p class=\"MessageTitle\" style=\"width: 80px\">漢語大字典</p> \n" +
                        "  <dl style=\"width: 95%\" class=\"control\"> \n" +
                        "   <dd class=\"size\">\n" +
                        "    《說文》：“馬，怒也；武也。象馬頭髦尾四足之形。\n" +
                        "    <img src=\"/ImageText2/ShowImage.ashx?text=%f0%a2%92%a0&amp;font=%e7%b4%b0%e6%98%8e%e9%ab%94&amp;size=13&amp;style=regular&amp;color=%23000000\" class=\"NtSize\" style=\"vertical-align:text-bottom\">，古文。\n" +
                        "    <img src=\"/ImageText2/ShowImage.ashx?text=%f0%a2%92%a0&amp;font=%e7%b4%b0%e6%98%8e%e9%ab%94&amp;size=13&amp;style=regular&amp;color=%23000000\" class=\"NtSize\" style=\"vertical-align:text-bottom\">，籀文馬與\n" +
                        "    <img src=\"/ImageText2/ShowImage.ashx?text=%f0%a2%92%a0&amp;font=%e7%b4%b0%e6%98%8e%e9%ab%94&amp;size=13&amp;style=regular&amp;color=%23000000\" class=\"NtSize\" style=\"vertical-align:text-bottom\">同，有髦。”\n" +
                        "   </dd> \n" +
                        "  </dl> \n" +
                        "  <dl style=\"width: 95%\" class=\"control\"> \n" +
                        "   <dd style=\"text-align: right\">\n" +
                        "    字體：\n" +
                        "    <a id=\"smallSize\" class=\"size\">小</a>．\n" +
                        "    <a id=\"middleSize\" class=\"size\">中</a>．\n" +
                        "    <a id=\"largeSize\" class=\"size\">大</a>\n" +
                        "   </dd> \n" +
                        "  </dl> \n" +
                        "  <p class=\"MessageTitle\" style=\"width: 64px\">相關連結</p> \n" +
                        "  <ul style=\"width: 95%\"> \n" +
                        "   <li>小學堂<a href=\"http://xiaoxue.iis.sinica.edu.tw/variants?kaiOrder=1955\" target=\"_blank\">異體字表</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/xiaozhuan?kaiOrder=1955\" target=\"_blank\">小篆</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/jinwen?kaiOrder=1955\" target=\"_blank\">金文</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/jiaguwen?kaiOrder=1955\" target=\"_blank\">甲骨文</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/chuwenzi?kaiOrder=1955\" target=\"_blank\">楚系簡帛文字</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/qinwenzi?kaiOrder=1955\" target=\"_blank\">秦系簡牘文字</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/chuanchao?kaiOrder=1955\" target=\"_blank\">傳抄古文字</a></li> \n" +
                        "   <li>小學堂<a href=\"http://xiaoxue.iis.sinica.edu.tw/shangguyin?kaiOrder=1955\" target=\"_blank\">上古音</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/zhongguyin?kaiOrder=1955\" target=\"_blank\">中古音</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/guanhua?kaiOrder=1955\" target=\"_blank\">官話</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/jinyu?kaiOrder=1955\" target=\"_blank\">晉語</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/wuyu?kaiOrder=1955\" target=\"_blank\">吳語</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/ganyu?kaiOrder=1955\" target=\"_blank\">贛語</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/xiangyu?kaiOrder=1955\" target=\"_blank\">湘語</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/minyu?kaiOrder=1955\" target=\"_blank\">閩語</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/yueyu?kaiOrder=1955\" target=\"_blank\">粵語</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/pinghua?kaiOrder=1955\" target=\"_blank\">平話</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/keyu?kaiOrder=1955\" target=\"_blank\">客語</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/otherdialects?kaiOrder=1955\" target=\"_blank\">其他土話</a></li> \n" +
                        "   <li>小學堂<a href=\"http://xiaoxue.iis.sinica.edu.tw/yunshu?kaiOrder=1955\" target=\"_blank\">韻書集成</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/guangyun?kaiOrder=1955\" target=\"_blank\">廣韻</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/jiyun?kaiOrder=1955\" target=\"_blank\">集韻</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/libu?kaiOrder=1955\" target=\"_blank\">附釋文互註禮部韻略</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/zengyun?kaiOrder=1955\" target=\"_blank\">增修互注禮部韻略</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/zhongyuan?kaiOrder=1955\" target=\"_blank\">中原音韻</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/zhongzhou?kaiOrder=1955\" target=\"_blank\">中州音韻</a>、<a href=\"http://xiaoxue.iis.sinica.edu.tw/hongwu?kaiOrder=1955\" target=\"_blank\">洪武正韻</a></li> \n" +
                        "   <li><a href=\"http://chardb.iis.sinica.edu.tw/char/U+99AC\" target=\"_blank\">國際電腦漢字與異體字知識庫</a>（含漢語大字典釋義）</li> \n" +
                        "   <li>教育部<a href=\"http://dict.variants.moe.edu.tw/yitia/fra/fra04619.htm\" target=\"_blank\">異體字字典</a></li> \n" +
                        "   <li><a href=\"http://kangxi.adcs.org.tw/kangxizidian/#%E9%A6%AC\" target=\"_blank\">開放康熙字典</a></li> \n" +
                        "   <li><a href=\"http://lib.ctcn.edu.tw/chtdict/result.aspx?keyword=%E9%A6%AC\" target=\"_blank\">遠流活用中文大辭典</a></li> \n" +
                        "  </ul> \n" +
                        "  <p class=\"MessageTitle\" style=\"width: 64px\">相關索引</p> \n" +
                        "  <ul style=\"width: 95%\"> \n" +
                        "   <li>漢語大字典（遠東圖書公司），冊.頁.字：7.4539.1</li> \n" +
                        "   <li>漢語大字典（建宏出版社），頁.字：1886.17</li> \n" +
                        "   <li>康熙字典（中華書局），頁.字：1433.01</li> \n" +
                        "   <li>中文大辭典（中華學術院），冊.編號：10.45550</li> \n" +
                        "   <li>說文解字（中華書局），卷.部首：10上.馬部</li> \n" +
                        "   <li>說文解字詁林正補合編（鼎文書局），冊.頁：8.383</li> \n" +
                        "   <li>說文新證（藝文印書館），冊頁：下98</li> \n" +
                        "   <li>說文新證（福建人民出版社），頁：770</li> \n" +
                        "   <li>金文編（中華書局），頁.行：675.3</li> \n" +
                        "   <li>金文詁林（香港中文大學），卷.頁：10.1</li> \n" +
                        "   <li>金文詁林補（中央研究院），冊.卷.頁：5.10.3073</li> \n" +
                        "   <li>殷周金文集成引得（中華書局），字號.頁：3538.1060</li> \n" +
                        "   <li>新金文編（作家出版社），冊頁：中1387</li> \n" +
                        "   <li>甲骨文編（中華書局），頁.行：397.3</li> \n" +
                        "   <li>甲骨文字詁林（吉林大學），冊.頁：2.1589</li> \n" +
                        "   <li>甲骨文字集釋（中央研究院），卷.頁：10.3031</li> \n" +
                        "   <li>殷墟甲骨刻辭類纂（吉林大學），冊頁：中624</li> \n" +
                        "   <li>新甲骨文編（福建人民出版社），頁.行：539.3</li> \n" +
                        "   <li>甲骨文字編（中華書局），字號.冊頁：1928.中573</li> \n" +
                        "   <li>楚系簡帛文字編（增訂本）（湖北教育出版社），頁.行：853.2</li> \n" +
                        "   <li>睡虎地秦簡文字編（文物出版社），頁.行：153.2</li> \n" +
                        "   <li>秦簡牘文字編（福建人民出版社），頁：289</li> \n" +
                        "   <li>戰國古文字典：戰國古文聲系（中華書局），冊頁：上605</li> \n" +
                        "   <li>傳抄古文字編（綫裝書局），冊頁.行：下963.1</li> \n" +
                        "  </ul> \n" +
                        " </body>\n" +
                        "</html>\n"
        );
    }


    /** Sourced from a Taiwanese university's Chinese character etymology database, XiaoXue (URL listed in Jsoup.connect()).
     *  By specifying mocking=true, a mocked response is used in lieu of sending a request to the XiaoXue website.
     *
     *  Thanks http://www.mkyong.com/java/jsoup-check-redirect-url/ for 'Response' class.
     *  Returns a list of 'CharacterEntry', which aggregates the entry's image URL along with any data about the
     *  script type and era that can be found in XiaoXue's provided description.
     *
     *  Note: undeterminate behaviour if there are more than 36 (one page of) results. Haven't found an example yet to test .*/
    public List<CharacterEntry> getPNGsFromXiaoXue(boolean mocking) throws IOException {
        Document document;
        if (mocking) {
            document = mockResponseFromXiaoXue();
        } else {
            // Searches for kanjiToSearch on the 'character evolution' section of the XiaoXue website.
            Response response = Jsoup.connect("http://xiaoxue.iis.sinica.edu.tw/yanbian/PageResult/PageResult")
            .data("ZiOrder", "")
            .data("EudcFontChar", kanjiToSearch)
            .data("ImageSize", MAX_RESULTS)
            .data("X-Requested-With", "XMLHTTPRequest")
            .method(Connection.Method.POST)
            .timeout(GENEROUS_TIMEOUT)
            .execute();

            document = response.parse();
        }

        document.charset(StandardCharsets.UTF_16);

        // Selects all the tables out of the mock or real response, then sub-selects imgURLs from rows containing characters.
        Elements tables = document.select("tr");
        Elements rowsWithEntries = tables.select("td.VariantListA, td.VariantListB");
        Elements imgs = rowsWithEntries.select("img");


        // The first image returned is always the modern character form.
        String modernChar;
        if(mocking){
            modernChar = "http://xiaoxue.iis.sinica.edu.tw" + tables.select("img").first().attr("src");
        }
        else{
            if(rowsWithEntries.isEmpty()){ return null; }
            modernChar = tables.select("img").first().attr("abs:src");
        }

        // Adds to a CharacterEntry List, using from the response all the img URLs and their corresponding descriptive text.
        List<CharacterEntry> characterEntries = new ArrayList<>();
        for(int i = 0; i < imgs.size(); i++){
            CharacterEntry XiaoXueElement;
            if(mocking){
                // ie.  http://xiaoxue.iis.sinica.edu.tw + /ImageText2/ShowImage.ashx?text=&font=中研院甲骨文重文三&size=36&style=regular&color=%23000000
                XiaoXueElement = new CharacterEntry("http://xiaoxue.iis.sinica.edu.tw" + imgs.get(i).attr("src"), rowsWithEntries.get(i).text());
            }
            else{
                XiaoXueElement = new CharacterEntry(imgs.get(i).attr("abs:src"), rowsWithEntries.get(i).text());
            }

            characterEntries.add(XiaoXueElement);
        }

        // Will only add the modern character if submitSearch results were received (if imgs List is non-empty).
        if(imgs.size() > 0){

            characterEntries.add(new CharacterEntry(modernChar, "prototype"));
        }

        return characterEntries;
    }
}
