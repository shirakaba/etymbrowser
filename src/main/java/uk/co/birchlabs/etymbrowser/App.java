package uk.co.birchlabs.etymbrowser;

import javax.swing.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/** This program is an English-language, user-friendly GUI for the XiaoXue website, which translates
 *  Main class. Starts both the main thread and the graphical thread. A minimum window size of 640 * 640 px is expected. */
public class App
{
    // Thread pool prepared for requesting images from a server in parallel later on.
    public static ExecutorService executor = Executors.newFixedThreadPool(36);
    public static final int ACCEPTABLE_V_SCROLLSPEED = 20;
    public static final int ACCEPTABLE_H_SCROLLSPEED = 50;
    private static final int SUITABLE_WIN_WIDTH = 640;
    private static final int SUITABLE_WIN_HEIGHT = 640;

    public static void main( String[] args ) {
        App program = new App();
        SwingUtilities.invokeLater(program::run);
    }


    private void run() {
        // This disables the default 'bold' style for text to enable style differences in HTML formatted text.
        UIManager.put("swing.boldMetal", false);

        JFrame w = new JFrame();
        w.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        w.setTitle("Chinese character etymology");
        w.add(new Display());
        w.pack();
        w.setSize(SUITABLE_WIN_WIDTH, SUITABLE_WIN_HEIGHT);
        w.setLocationByPlatform(true);
        w.setVisible(true);
    }
}
