package uk.co.birchlabs.etymbrowser;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

/** The entire graphical display for the program, wrapped into one JPanel. Consists of a submitSearch UI, results panel,
 *  and row of example inputs.*/
public class Display extends JPanel {
    private static final int COLS = 1;
    private static final String FULL_WIDTH = "width :100%";
    private static final String FULL_HEIGHT = "height :100%";

    public Display() {
        setLayout(new MigLayout(
                String.format("wrap %d", COLS),
                "", // cols
                "[][fill]")); // rows

        // Search UI
        MainContent theMainContent = new MainContent();
        SearchUI theSearchUI = new SearchUI(theMainContent);
        JTextField theTextField = theSearchUI.getTextField();
        add(theSearchUI);

        // Results
        JPanel results = new JPanel();
        results.setLayout(new MigLayout());
        String fullWidthFullHeight = String.format("%s, %s", FULL_WIDTH, FULL_HEIGHT);
        results.add(theMainContent, fullWidthFullHeight);
        add(results, fullWidthFullHeight);

        // Example inputs chosen for
        List<SimpleEntry<String, String>> examplesList = new ArrayList<>();
        examplesList.add(new SimpleEntry<>("House", "家"));
        examplesList.add(new SimpleEntry<>("Fish", "魚"));
        examplesList.add(new SimpleEntry<>("Elephant", "象"));
        examplesList.add(new SimpleEntry<>("Cow", "牛"));
        examplesList.add(new SimpleEntry<>("Vehicle", "車"));
        examplesList.add(new SimpleEntry<>("Horse", "馬"));
        examplesList.add(new SimpleEntry<>("Bird", "鳥"));
        JScrollPane examplesRow = new HorizontallyScrollingPane(new ExamplesPanel(examplesList, theTextField));
        add(examplesRow);
    }
}
