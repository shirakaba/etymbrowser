package uk.co.birchlabs.etymbrowser;

import javax.swing.*;
import java.awt.*;

import static uk.co.birchlabs.etymbrowser.App.ACCEPTABLE_H_SCROLLSPEED;

/** An exclusively horizontally scrolling pane that views an input JPanel.
 */
public class HorizontallyScrollingPane extends JScrollPane {

    public HorizontallyScrollingPane(JPanel content) {
        super(content,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        Rectangle bounds = getViewport().getViewRect();

        JScrollBar vertical = getVerticalScrollBar();
        vertical.setValue( (vertical.getMaximum() - bounds.height) / 2 );
        getHorizontalScrollBar().setUnitIncrement(ACCEPTABLE_H_SCROLLSPEED);
        setMinimumSize(new Dimension(1, content.getMinimumSize().height));
    }

}
