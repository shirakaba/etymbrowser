package uk.co.birchlabs.etymbrowser;

import java.lang.UnsupportedOperationException;

import java.awt.*;

/** A class holding all the data for a Chinese character extracted from the XiaoXue website, which includes its image URL,
 *  its raw description, and from that description what type of Script (medium and era) it must be.
 */
public class CharacterEntry {
    private final String URL;
    private final String description;
    private final Script whatScript;


    public CharacterEntry(String URL, String description) {
        this.URL = URL;
        this.description = description;
        this.whatScript = determineScript();
    }


    public String getURL() {
        return URL;
    }


    /** Determines the category of the script based on the Chinese description from XiaoXue.
     *  Returns: Oracle; Bronze (subdivided into early/mid/late/unknown periods of Shang, Western Zhou, Spring & Autumn,
     *  Warring States, and uncategorised); Chu bamboo/silk; old small seal; big seal; newer small seal; and Qin.*/
    private Script determineScript() {
        if(description.contains("prototype")){ return Script.MODERN; }
        if(description.contains("(甲)")){ return Script.ORACLE; }
        // The Bronze medium has sub-categories and starts here:
        if(description.contains("(金)")){
            if(description.endsWith("商代早期")){ return Script.B_SHANG_EARLY; }
            if(description.endsWith("商代中期")){ return Script.B_SHANG_MID; }
            if(description.endsWith("商代晚期")){ return Script.B_SHANG_LATE; }
            if(description.endsWith("商代")){ return Script.B_SHANG; }

            if(description.endsWith("西周早期")){ return Script.B_WZHOU_EARLY; }
            if(description.endsWith("西周中期")){ return Script.B_WZHOU_MID; }
            if(description.endsWith("西周晚期")){ return Script.B_WZHOU_LATE; }
            if(description.endsWith("西周")){ return Script.B_WZHOU; }

            if(description.endsWith("春秋早期")){ return Script.B_SA_EARLY; }
            if(description.endsWith("春秋中期")){ return Script.B_SA_MID; }
            if(description.endsWith("春秋晚期")){ return Script.B_SA_LATE; }
            if(description.endsWith("春秋")){ return Script.B_SA; }

            if(description.endsWith("戰國早期")){ return Script.B_WS_EARLY; }
            if(description.endsWith("戰國中期")){ return Script.B_WS_MID; }
            if(description.endsWith("戰國晚期")){ return Script.B_WS_LATE; }
            if(description.endsWith("戰國")){ return Script.B_WS; }

            return Script.BRONZE; }
        if(description.contains("(楚)")){ return Script.CHU_BAMBOO_SILK; }
        if(description.contains("說文古文")){ return Script.OLD_SMALL_SEAL; }
        if(description.contains("說文籀文")){ return Script.SHUOWEN_BIG_SEAL; }
        if(description.startsWith("說文") && description.endsWith("部")){ return Script.NEWER_SMALL_SEAL; } // 說文‧大部 or 說文‧王部
        if(description.contains("(秦)")){ return Script.QIN_BAMBOO; }

        return Script.OTHER;
    }


    /** Returns whatever script type the entry from XiaoXue was determined to be. */
    public String getScript() {
        switch (whatScript) {
            case ORACLE:
                return "Oracle bone";
            case BRONZE:
            case B_SHANG_EARLY:
            case B_SHANG_MID:
            case B_SHANG_LATE:
            case B_SHANG:
            case B_WZHOU_EARLY:
            case B_WZHOU_MID:
            case B_WZHOU_LATE:
            case B_WZHOU:
            case B_SA_EARLY:
            case B_SA_MID:
            case B_SA_LATE:
            case B_SA:
            case B_WS_EARLY:
            case B_WS_MID:
            case B_WS_LATE:
            case B_WS:
                return "Bronze";
            case SHUOWEN_BIG_SEAL:
                return "Large seal";
            case LST_BIG_SEAL:
                return "Large seal from Sears";
            case SMALL_SEAL:
                return "Small seal from Sears";
            case CHU_BAMBOO_SILK:
                return "Bamboo/silk Chu";
            case OLD_SMALL_SEAL:
                return "Older small seal";
            case NEWER_SMALL_SEAL:
                return "Newer small seal";
            case QIN_BAMBOO:
                return "Clerical bamboo Qin";
            case MODERN:
                return "Modern form";
            case OTHER:
                return "Uncategorisable.";
            default:
                throw new UnsupportedOperationException();
        }
    }


    /** Returns a reminiscent colour of the script's medium (eg. orange colour for the 'bronze' medium) */
    public Color getColour() {
        switch (whatScript) {
            case ORACLE:
                return Color.decode("0xf9f9de");
            case BRONZE:
            case B_SHANG_EARLY:
            case B_SHANG_MID:
            case B_SHANG_LATE:
            case B_SHANG:
            case B_WZHOU_EARLY:
            case B_WZHOU_MID:
            case B_WZHOU_LATE:
            case B_WZHOU:
            case B_SA_EARLY:
            case B_SA_MID:
            case B_SA_LATE:
            case B_SA:
            case B_WS_EARLY:
            case B_WS_MID:
            case B_WS_LATE:
            case B_WS:
                return Color.decode("0xe58819");
            case SHUOWEN_BIG_SEAL:
            case LST_BIG_SEAL:
            case SMALL_SEAL:
            case OLD_SMALL_SEAL:
                return Color.decode("0xa3a3a3");
            case CHU_BAMBOO_SILK:
            case QIN_BAMBOO:
                return Color.decode("0xe5e4be");
            case NEWER_SMALL_SEAL:
                return Color.decode("0xbfbfbf");
            case MODERN:
            case OTHER:
                return Color.decode("0xFFFFFF");
            default:
                throw new UnsupportedOperationException();
        }
    }


    /** Returns a brief description of what time period and dynasties the character's picture would have come from. */
    public String getDate() {
        switch (whatScript) {
            case ORACLE:
                return "1600-770 BC" + "<br>Shang,<br>Western Zhou";
            case BRONZE:
                return "1600-250 BC";

            case B_SHANG_EARLY:
                return " 1600-1050 BC (early)" + "<br>Shang";
            case B_SHANG_MID:
                return " 1600-1050 BC (mid)" + "<br>Shang";
            case B_SHANG_LATE:
                return " 1600-1050 BC (late)" + "<br>Shang";
            case B_SHANG:
                return " 1600-1050 BC" + "<br>Shang";

            case B_WZHOU_EARLY:
                return " 1050-770 BC (early)" + "<br>Western Zhou";
            case B_WZHOU_MID:
                return " 1050-770 BC (mid)" + "<br>Western Zhou";
            case B_WZHOU_LATE:
                return " 1050-770 BC (late)" + "<br>Western Zhou";
            case B_WZHOU:
                return " 1050-770 BC" + "<br>Western Zhou";

            case B_SA_EARLY:
                return " 770-479 BC (early)" + "<br>Spring & Autumn";
            case B_SA_MID:
                return " 770-479 BC (mid)" + "<br>Spring & Autumn";
            case B_SA_LATE:
                return " 770-479 BC (late)" + "<br>Spring & Autumn";
            case B_SA:
                return " 770-479 BC" + "<br>Spring & Autumn";

            case B_WS_EARLY:
                return " 476-221 BC (early)" + "<br>Warring States";
            case B_WS_MID:
                return " 476-221 BC (mid)" + "<br>Warring States";
            case B_WS_LATE:
                return " 476-221 BC (late)" + "<br>Warring States";
            case B_WS:
                return " 476-221 BC" + "<br>Warring States";

            case SHUOWEN_BIG_SEAL:
                return " 770-221 BC" + "<br>Eastern Zhou or<br>Warring States";
            case LST_BIG_SEAL:
                return "Undefined";
            case SMALL_SEAL:
                return "Undefined";
            case CHU_BAMBOO_SILK:
                return " 476-221 BC" + "<br>Warring States"; // according to p26 Buried Ideas: Legends of Abdication and Ideal Government in Early Chinese...
            case OLD_SMALL_SEAL:
                return " 476-221 BC" + "<br>Warring States"; // I originally included Qin, but only evidence is the rikai-kun lookup of 'late Zhou, pre-Han'.
            case NEWER_SMALL_SEAL:
                return "206 BC - 25 AD" + "<br>Western Han";
            case QIN_BAMBOO:
                return " 221-206 BC" + "<br>Qin";
            case MODERN:
                return "557 AD~" + "<br>Tang onwards";
            case OTHER:
                return "Uncategorisable.";
            default:
                throw new UnsupportedOperationException();
        }
    }


    /** Returns a thorough description of the historical context of the CharacterEntry. */
    public String getDescription() {
        switch (whatScript) {
            case ORACLE:
                return "Characters inscribed on animal bones for divination, used overwhelmingly throughout the Shang " +
                        "(1600-1050 BC), but occasionally (<0.2% of finds) during the Western Zhou period (1050-770 BC).";
            case BRONZE:
            case B_SHANG_EARLY:
            case B_SHANG_MID:
            case B_SHANG_LATE:
            case B_SHANG:
            case B_WZHOU_EARLY:
            case B_WZHOU_MID:
            case B_WZHOU_LATE:
            case B_WZHOU:
            case B_SA_EARLY:
            case B_SA_MID:
            case B_SA_LATE:
            case B_SA:
            case B_WS_EARLY:
            case B_WS_MID:
            case B_WS_LATE:
            case B_WS:
                return "Characters inscribed in bronze, used predominantly throughout the Shang (1600-1050 BC) and Zhou periods (1050-250 BC).";
            case LST_BIG_SEAL:
                return "'Large seal' characters used throughout the Eastern Zhou period (770-250 BC)."; // Sears only.
            case SMALL_SEAL:
                return "'Small seal' characters used during the Warring states (476-221 BC) and Western Han (206 BC - 25 AD)" +
                        " dynasties. Developed alongside the modern 'lishu' clerk script even since their initial conception.";
            case CHU_BAMBOO_SILK:
                return "Characters written on bamboo or silk dated to the middle (~300 BC) of the Warring States (476-221 BC)" +
                        " period, found in a tomb in the state of Chu and not included in the Shuowen etymology dictionary.";
            case OLD_SMALL_SEAL: // discovered in manuscripts dated to ~150 BC (early Han)
                return "'Old small seal' entries from the Warring States period (475-221 BC) used in the other six " +
                        "states excluding Qin, listed in the Shuowen etymology dictionary. " +
                        "There may be some temporal overlap with the 'large seal' characters, but the history " +
                        "seems unclear.";
            case SHUOWEN_BIG_SEAL: // 770-221 BC
                return "'Large seal' (大篆) forms detailed in the lost Shizhoupian dictionary from the state of Qin - which is dated either to " +
                        "Eastern Zhou (770-250 BC) or the Warring States period (476-221 BC) - recovered through quotes in the Shuowen " +
                        "etymology dictionary where forms differed from small seal style. More complex and rounded than small seal. " +
                        "These characters evolved independently in the six different warring states until, upon the Qin conquest," +
                        " being discarded except for where they matched the Qin state's form.";
            case NEWER_SMALL_SEAL: // argued as contemporary to 100AD if not Qin (220-206 BC)
                return "Post-Qin 'Small seal' (小篆) still-not-quite standardised forms of the Western Han era (206 BC - 25 AD) as recorded in the Shuowen etymology" +
                        " dictionary. Use was restricted largely to signet seals and the titles of stelae (inscribed stone " +
                        "memorial tablets), rather than the contemporaneous 'clerical bamboo script' which came to enjoy common use.";
            case QIN_BAMBOO:
                return "Clerical script on bamboo found in its mature stage dated to the Qin dynasty (221-206 BC); this script" +
                        " style developed alongside the more formal 'small seal' styles, gaining favour as writing became more commonplace." +
                        " After the Qin fell, this script remained while seals cript disappeared from general use. " +
                        "As the first 'jinwen 今文'/'likai 隸楷' character, this is the direct ancestor of our modern characters.";
            case MODERN:
                return "Modern day traditional characters, represented by printed type. While bronze movable type only became available" +
                        " in China from the Jin dynasty (1115-1234 AD), this form of the characters has remained largely" +
                        " unchanged since the early Tang dynasty, specifically pointing to the works of calligrapher " +
                        "Ouyang Xun (557-641 AD) as the first mature regular script.";
            case OTHER:
                return "Uncategorisable.";
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    public String toString(){
        return "[ " + URL + " , " + description + " ]";
    }
}
