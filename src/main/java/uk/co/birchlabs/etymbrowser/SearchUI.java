package uk.co.birchlabs.etymbrowser;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;

/** This class consists of all the Search UI: a searchbar with label and button, a status label, and
 *  (proof of concept, non-operational) checkboxes for search customisation. */
public class SearchUI extends JPanel {
    private static final String DEFAULT_CHAR = "馬";
    private static final String ROWS = "2";
    private static final String COLS = "3";
    private static final int TO_MSECS = 1000;
    private static final int FREQ_OF_TICKING = 250;
    private static final int DELAY = 100;
    private final JLabel status;
    private final JTextField textField = new JTextField(DEFAULT_CHAR);
    private final MainContent mainContent;

    /** Constructs the main JPanel, taking a reference of the MainContent to display results from searches. */
    public SearchUI(MainContent mainContent) {
        this.mainContent = mainContent;

        setLayout(new MigLayout());

        // Label for searchbar
        add(new JLabel("Chinese character:"),
                "spany " + ROWS);

        // Searchbar itself
        textField.addActionListener(this::action);
        add(textField,
                "growx, " +
                "spanx " + COLS);

        // Search button
        JButton searchButton = new JButton("Search");
        searchButton.addActionListener(this::action);
        add(searchButton,
                "spany " + ROWS + ", " +
                "split " + ROWS + ", " +
                "flowy");

        // Status label
        status = new JLabel(" ");
        status.setFont(new Font(status.getFont().getName(), Font.ITALIC, status.getFont().getSize()));
        add(status, "wrap");

        // Checkboxes
        add(new JCheckBox("小學堂"));
        add(new JCheckBox("Wiki"));
        add(new JCheckBox("Sears"), "wrap");
    }


    public JTextField getTextField() {
        return textField;
    }


    /** Submits the textfield's search query and sets the status label's text according to the search's status. */
    private void action(ActionEvent e) {
        status.setText("searching...");

        // Animates the '...' after 'searching' to show that the program isn't frozen.
        Timer t = new Timer(DELAY, e1 -> {
            int msecs = (int) (e1.getWhen() % TO_MSECS);
            int dots = msecs/ FREQ_OF_TICKING;
            String dotsStr = new String(new char[dots]).replace("\0", ".");
            status.setText(String.format("searching%s", dotsStr));
        });
        t.start();

        // Passes several operations to an anonymous class fulfilling the Runnable interface:
        // the command for mainContent to submit the textfield's search query, and to set the resulting status message.
        App.executor.submit((Runnable) () -> {
            boolean success = mainContent.submitSearch(textField.getText());
            t.stop();
            if(success){
                status.setText("finished.");
            }
            else {
                status.setText("no results.");
            }
        });
    }
}
