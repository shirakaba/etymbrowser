package uk.co.birchlabs.etymbrowser;

import net.miginfocom.swing.MigLayout;
import java.lang.UnsupportedOperationException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.BorderUIResource;

import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static uk.co.birchlabs.etymbrowser.App.ACCEPTABLE_H_SCROLLSPEED;
import static uk.co.birchlabs.etymbrowser.App.ACCEPTABLE_V_SCROLLSPEED;

/** This class is a scrollable pane viewing a JPanel which initially displays a text tutorial, but upon submitting and receiving a
 *  successful response to a search query, displays a table of CharacterEntry images and corresponding information about
 *  their Scripts, Dates, and Descriptions.
 */
public class MainContent extends JScrollPane {
    private List<CharacterEntry> imgURLs;
    private final Container table;
    private static final int COLS = 4;

    public MainContent() {
        // JScrollPane views a subclass of Component, which we extract as a variable 'table' in order to set its layout.
        super(new JPanel());
        Component view = this.getViewport().getView();
        if (!(view instanceof Container)) {
            throw new IllegalStateException("Expected the Component assigned to my viewport's view, to be castable to Container.");
        }
        table = (Container) view;
        table.setLayout(new MigLayout(
                String.format("wrap %d", COLS),
                "[left][][][fill, grow]", // cols
                "[top][]")); // rows

        String tutorial =
                "<b>**WELCOME**</b><br>This is an English-language interface for the Chinese character etymology Search engine 'XiaoXue', " +
                "produced by the Taiwanese academic institution Academia Sinica (中央研究院).<br><br><i>NOTE: if the text between " +
                "those brackets doesn't display properly, Chinese language support is not configured on your computer.</i>" +
                "<br><br><br>" +
                "<b>**HOW TO USE**</b><br>Simply write a single Chinese character into the Search field " +
                "(or click one of the example hyperlinks below) " +
                "and press the " +
                "Search button. The checkboxes below the Search field are non-operational and merely proof-of-concept for a future capability to " +
                "combine resources from different databases.<br><br><i>NOTE: supports traditional Chinese characters (and those shared with Japanese), " +
                "but not simplified.</i>";

        table.add(new WrappingJLabel(tutorial), String.format("grow, spanx %d", COLS));

        // contentScrollPane is a view of the JPanel 'table'.
        getVerticalScrollBar().setUnitIncrement(ACCEPTABLE_V_SCROLLSPEED);
        getHorizontalScrollBar().setUnitIncrement(ACCEPTABLE_H_SCROLLSPEED);
    }


    /** Sends a string as a search query and, if valid results are received, clears and re-populates the table with the
     *  images and descriptions of Chinese characters from those results, then finally returns TRUE.
     *
     *  If no results are received, returns FALSE and leaves the table uncleared. */
    public boolean submitSearch(String query){
        if(query == null || query.equals("")){
            return false;
        }

        ImgURLExtractor extractor = new ImgURLExtractor(query);
        try {
            imgURLs = extractor.getPNGsFromXiaoXue(false);
        } catch (IOException e) {
            System.err.println(String.format("query '%s' didn't produce a valid URL for XiaoXue, or the request timed out.", query));
            imgURLs = null;
        }

        if(imgURLs == null){
            return false;
        }

        table.removeAll();
        populateTable();
        getVerticalScrollBar().setValue(0); // scrolls back to top.
        getHorizontalScrollBar().setValue(0); // scrolls back to left.
        updateUI();

        return true;
    }


    private enum SpanYCalcMode{
        GETSCRIPT,
        GETCOLOUR,
        GETDATE,
        GETDESCRIPTION
    }


    /** Calculates the spany for each cell based on whether they contain the same data as the next cell beneath them.
     *  Returns an Integer List of all the spanys necessary to render a column of data such as Script, Colour, Description,
     *  or Date (specifiable by the SpanYCalcMode enum) without any redundant adjacent cells. */
    private List<Integer> spanYCalc(SpanYCalcMode spanYCalcMode){
        int spanY = 1;
        List<Integer> spanYList = new ArrayList<>();

        for(int i = 0; i + 1 < imgURLs.size(); i++){
            String currentData, nextData;

            switch (spanYCalcMode) {
                case GETSCRIPT:
                    currentData = imgURLs.get(i).getScript();
                    nextData = imgURLs.get(i + 1).getScript();
                    break;
                case GETCOLOUR:
                    currentData = imgURLs.get(i).getColour().toString();
                    nextData = imgURLs.get(i + 1).getColour().toString();
                    break;
                case GETDESCRIPTION:
                    currentData = imgURLs.get(i).getDescription();
                    nextData = imgURLs.get(i + 1).getDescription();
                    break;
                case GETDATE:
                    currentData = imgURLs.get(i).getDate();
                    nextData = imgURLs.get(i + 1).getDate();
                    break;
                default:
                    throw new UnsupportedOperationException();
            }


            if(currentData.equals(nextData)){
                spanY++;
            }
            else{
               spanYList.add(spanY);
               spanY = 1;
            }
        }

        spanYList.add(spanY);

        return spanYList;
    }


    /** Downloads CharacterEntry images into memory, then calls addComponentsBySpanY() to add them along with their
     *  corresponding data (such as Script, Colour, Description, and Date) as rows to the table, with each data cell spanning
     *  multiple rows if rows directly below them are identical, in lieu of displaying duplicate data cells. */
    private void populateTable() {
        // multi-threading references https://stackoverflow.com/questions/3142915/how-do-you-create-an-asynchronous-http-request-in-java
        // Create a List of Images to be downloaded from URLs.
        List<Future<Image>> imageFutures = imgURLs.stream() // see http://stackoverflow.com/questions/28319064/java-8-best-way-to-transform-a-list-map-or-foreach
                .map(CharacterEntry::getURL) // AKA: imgURL -> imgURL.getURL()
                // Promotes the strings to URLs.
                .map(str -> {
                    URL url = null;
                    try {
                        url = new URL(str);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    return url;
                })
                // Filters nulls out of URL collection.
                .filter(p -> p != null)
                // Submits jobs to the thread pool.
                .map(url -> App.executor.submit((Callable<Image>) () -> ImageIO.read(url)))
                .collect(Collectors.toList());

        // Waits upon the images all having been downloaded before continuing.
        List<Image> images = imageFutures.stream()
                .map(imageFuture -> {
                    Image image = null;
                    try {
                        image = imageFuture.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                    return image;
                })
                .filter(p -> p != null)
                .collect(Collectors.toList());

        addComponentsBySpanY(images);
    }


    /** Adds each row of CharacterEntry information to the table (Script, Description, Date), specifying the correct 'spany'
     *  to effectively vertically merge any identical cells and setting an appropriate colour for the Script cells. */
    private void addComponentsBySpanY(List<Image> images) {
        // Calculates the number of rows JLabels will need to span so as not to write redundant JLabels one after another.
        List<Integer> scriptSpanYs = spanYCalc(SpanYCalcMode.GETSCRIPT);
        List<Integer> descriptionSpanYs = spanYCalc(SpanYCalcMode.GETDESCRIPTION);
        List<Integer> dateSpanYs = spanYCalc(SpanYCalcMode.GETDATE);

        Integer currentScriptSpanY, scriptBlock = 0,
                currentDescrSpanY, descrBlock = 0,
                currentDateSpanY, dateBlock = 0;

        for(int i = 0; i < imgURLs.size(); i++){
            table.add(new ImageComponent(images.get(i)), "aligny top");
            CharacterEntry currentEntry = imgURLs.get(i);

            // Adds a JLabel for each cell of information corresponding to the image, but only if no identical label needs
            // to span for further rows; 'blocks' are used to prevent adding of such labels prematurely.
            if(scriptSpanYs.size() > 0 && descriptionSpanYs.size() > 0 && dateSpanYs.size() > 0){
                // Get the prescribed spany for each cell.
                currentScriptSpanY = scriptSpanYs.get(0);
                currentDescrSpanY = descriptionSpanYs.get(0);
                currentDateSpanY = dateSpanYs.get(0);

                // JLabels are only to be added if they are not blocked from addition by a currently still-spanning JLabel.
                if(scriptBlock == 0 ) {
                    // Script label, ie. "Bronze (late)".
                    scriptBlock = addScriptLabel(scriptSpanYs, currentScriptSpanY, currentEntry);
                }

                if(dateBlock == 0 ) {
                    // Date label, ie. "100-200 BC".
                    dateBlock = addDateLabel(dateSpanYs, currentDateSpanY, currentEntry);
                }

                if(descrBlock == 0 ) {
                    // Description label, ie. "As recorded in the Shuowen...".
                    descrBlock = addDescrLabel(descriptionSpanYs, currentDescrSpanY, currentEntry);
                }

                // Each iteration, reduce the blocks by one, as a new row will have been spanned.
                scriptBlock -= 1;
                dateBlock -= 1;
                descrBlock -= 1;
            }
        }
    }


    /** Adds a bordered JLabel describing the CharacterEntry's historical context.
     *
     *  Returns the spany it just added the JLabel with, so that adding new description JLabels will be blocked for that number of
     *  iterations of JLabel-adding. */
    private Integer addDescrLabel(List<Integer> descriptionSpanYs, Integer currentDescrSpanY, CharacterEntry currentEntry) {
        JLabel label = new WrappingJLabel(currentEntry.getDescription());
        label.setBorder(new BorderUIResource.LineBorderUIResource(Color.BLACK, 1));
        table.add(label, "grow, spany " + currentDescrSpanY);

        descriptionSpanYs.remove(0);

        return currentDescrSpanY;
    }


    /** Adds a plain JLabel describing the CharacterEntry's time period.
     *
     *  Returns the spany it just added the JLabel with, so that adding new date JLabels will be blocked for that number of
     *  iterations of JLabel-adding. */
    private Integer addDateLabel(List<Integer> dateSpanYs, Integer currentDateSpanY, CharacterEntry currentEntry) {
        JLabel date = new JLabel("<html>" + currentEntry.getDate() + "</html>");
        date.setBackground(Color.WHITE);
        date.setOpaque(true);
        date.setVerticalAlignment(SwingConstants.TOP);
        table.add(date, "grow, spany " + currentDateSpanY);

        dateSpanYs.remove(0);

        return currentDateSpanY;
    }


    /** Adds a coloured JLabel describing the CharacterEntry's medium of script.
     *
     *  Returns the spany it just added the JLabel with, so that adding new script JLabels will be blocked for that number of
     *  iterations of JLabel-adding. */
    private Integer addScriptLabel(List<Integer> scriptSpanYs, Integer currentScriptSpanY, CharacterEntry currentEntry) {
        JLabel colouredJLabel = new JLabel("<html>" + currentEntry.getScript() + "</html>");
        colouredJLabel.setBackground(currentEntry.getColour());
        colouredJLabel.setOpaque(true);
        colouredJLabel.setVerticalAlignment(SwingConstants.TOP);
        table.add(colouredJLabel, "grow, spany " + currentScriptSpanY);

        scriptSpanYs.remove(0);

        return currentScriptSpanY;
    }
}
