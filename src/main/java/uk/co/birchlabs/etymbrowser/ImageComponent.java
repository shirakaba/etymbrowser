package uk.co.birchlabs.etymbrowser;

import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;


/** ImageComponents are simply extensions of JPanel consisting of an Image and an awareness of that Image's dimensions to
 *  allow trouble-free aligning. */
public class ImageComponent extends JPanel {
    private Image image;
    private static final int PLACEHOLDER_HEIGHT = 100;
    private static final int PLACEHOLDER_WIDTH = 100;

    /** Makes an image */
    public ImageComponent(Image image) {
        this.image = image;
        setMinimumSize(new Dimension(image.getWidth(this), image.getHeight(this)));
    }

    /** Makes a placeholder image that is a 100*100 px black square. */
    public ImageComponent() {
        this(new BufferedImage(PLACEHOLDER_WIDTH, PLACEHOLDER_HEIGHT, BufferedImage.TYPE_INT_RGB));
    }


    public void paintComponent(Graphics g0) {
        super.paintComponent(g0);
        Graphics2D g = (Graphics2D) g0;
        // this alternative drawImage() from http://stackoverflow.com/questions/13038411/how-to-fit-image-size-to-jframe-size
        g.drawImage(image, 0, 0, image.getWidth(this), image.getHeight(this), this);
    }
}
