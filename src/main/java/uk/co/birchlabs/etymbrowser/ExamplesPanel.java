package uk.co.birchlabs.etymbrowser;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.util.AbstractMap;
import java.util.List;

/** Builds a row of ExampleInputters into a JScrollPane-viewed JPanel, each one sharing a common JTextField to modify upon
 *  being clicked.
 *  The ExampleInputters are constructed from the SimpleEntry<String, String> List, where the first (key) String is used
 *  as the English label, while the second String is used as the corresponding Japanese label.
 */
public class ExamplesPanel extends JPanel {
    public ExamplesPanel(List<AbstractMap.SimpleEntry<String, String>> pairList, JTextField theTextField) {
        setLayout(new MigLayout());
        add(new JLabel("<html><b>**Examples**</b></html>"));

        for (AbstractMap.SimpleEntry<String, String> pair : pairList) {
            add(new Example(theTextField, pair.getKey(), pair.getValue()));
        }
    }
}
