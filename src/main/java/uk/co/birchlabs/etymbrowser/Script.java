package uk.co.birchlabs.etymbrowser;

/** Enum class of all the Script information (sometimes named by dynasty, sometimes by medium) provided by XiaoXue and
 *  other websites. Sources for the dates are based on consensuses between the various sources listed in report.pdf. */
public enum Script {
    ORACLE,
    BRONZE,

    B_SHANG_EARLY,
    B_SHANG_MID,
    B_SHANG_LATE,
    B_SHANG,

    B_WZHOU_EARLY,
    B_WZHOU_MID,
    B_WZHOU_LATE,
    B_WZHOU,

    B_SA_EARLY,
    B_SA_MID,
    B_SA_LATE,
    B_SA,

    B_WS_EARLY,
    B_WS_MID,
    B_WS_LATE,
    B_WS,

    LST_BIG_SEAL,
    SMALL_SEAL,
    CHU_BAMBOO_SILK,
    OLD_SMALL_SEAL,
    SHUOWEN_BIG_SEAL,
    NEWER_SMALL_SEAL,
    QIN_BAMBOO,
    MODERN,
    OTHER
}
