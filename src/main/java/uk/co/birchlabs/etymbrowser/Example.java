package uk.co.birchlabs.etymbrowser;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

/** Constructs a JPanel consisting of an English JLabel and a corresponding hyperlinked Japanese character JLabel, which
 *  upon being clicked sets a specified JTextField to that value to be sent as a search query. */
public class Example extends JPanel {
    public Example(JTextField textField, String eng, String jp) {
        setLayout(new MigLayout());
        add(new JLabel(eng + ":"));

        // from http://stackoverflow.com/questions/527719/how-to-add-hyperlink-in-jlabel
        JButton jpLink = new JButton();
        jpLink.setText(
                String.format(
                        "<html><font color=\"#000099\"><u>%s</u></font></html>",
                        jp
                )
        );
        jpLink.setBorder(BorderFactory.createEmptyBorder());
        jpLink.setToolTipText("Input '" + jp + "' to the textfield to be searched.");
        jpLink.addActionListener(e -> textField.setText(jp));
        jpLink.setCursor(new Cursor(Cursor.HAND_CURSOR));
        add(jpLink);
    }
}
