package uk.co.birchlabs.etymbrowser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.util.List;

/** These are unmeasured tests purely for printing out the results of image URL extraction requests. */
public class ImgURLExtractorTest {

    @Test
    public void paragraphGetJapaneseWithoutConversion() throws Exception {
        String jpToConvert = "練馬区";
        Document doc = Jsoup.connect("https://ja.wikipedia.org/wiki/" + jpToConvert).timeout(5000).get();
        Elements paragraphs = doc.select(".mw-content-ltr p");

        System.out.println(paragraphs.text());
    }


    /** Gets any category of .gif from Sears' website. */
    @Test
    public void searsExtract() throws Exception {
        ImgURLExtractor imgURLExtractor = new ImgURLExtractor("馬");
        List<String> strings = imgURLExtractor.getGIFsFromSears(Script.BRONZE);

        System.out.println(strings);
    }


    /** Note: currently returns the file page rather than the image itself. */
    @Test
    public void WikimediaPNGExtract() throws Exception {
        ImgURLExtractor imgURLExtractor = new ImgURLExtractor("馬");
        List<String> strings = imgURLExtractor.getPNGsFromWikimedia();

        System.out.println(strings);
    }

    /** Extracts the image information and descriptions from the XiaoXue website. */
    @Test
    public void XiaoXueExtract() throws Exception {
        ImgURLExtractor imgURLExtractor = new ImgURLExtractor("馬");
        List<CharacterEntry> PNGsFromXiaoXue = imgURLExtractor.getPNGsFromXiaoXue(false);

        System.out.println(PNGsFromXiaoXue);
    }


}
